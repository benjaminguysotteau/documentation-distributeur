Distribution carte
==================

Paiement par billet
-------------------

* [x] Le lecteur de billet doit refuser les billet d'un montant plus élévé que la sélection.

### Paiement OK

#### Distribution OK

* [x] Vérifier que la dépense est bien enregistrée dans la table de transaction.
* [x] Vérifier que la distribution est bien enregistrée dans la table de transaction.
* [ ] Vérifier la conformité du ticket.

#### Distribution NOK

* [x] Vérifier que la panne est detectée (envoi d'un message sur Optiweb).
* [x] Vérifier que le matériel est mis en panne.
* [x] Vérifier au retour à l'écran d'accueil et démarrage que l'achat de carte n'est plus disponible.
* [ ] Vérifier que le montant introduit par le client est disponible en crédit restant.
* [x] Vérifier que la dépense est bien enregistrée dans la table de transaction.
* [x] Vérifier que la distribution N'EST PAS enregistrée dans la table de transaction.
* [x] Vérifier que le process de crédit restant est effectué correctement par la suite.
* [ ] Vérifier la présence d'un message indiquant la perte du credit restant sous optiweb.
* [ ] TICKET, AVOIR ?


### Paiement NOK (panne du lecteur de billet, billet conservé)

* [ ] Vérifier la présence d'un message d'anomalie pour l'utilisateur.
* [ ] Vérifier l'écriture de la panne en base de donnée.
* [ ] Vérifier que le lecteur de billet ne sera plus utilisable.
* [ ] Vérifier la bonne gestion du crédit restant si possible.
* [ ] ÉCRITURE EN TABLE D'ENCAISSEMENT ?

#### Paiement en plusieurs fois car le client n'a pas assez de billets

* [ ] Vérifier que la dépense est bien enregistrée dans la table de transaction.


Paiement par CB
---------------

* [ ] Tester sans la bride de 1€.
* [ ] Tester le sans contact.
* [ ] Tester une vente partielle (vendu 2, distribué 1).

### Paiement OK

#### Distribution OK

* [ ] Vérifier que la dépense est bien enregistrée dans la table de transaction.
* [ ] Vérifier que la distribution est bien enregistrée dans la table de transaction.
* [ ] Vérifier la conformite du ticket justificatif.
* [ ] Vérifier l'impression du ticket CB.

#### distribution NOK

* [ ] Vérifier que la panne est detectée (envoi d'un message sur Optiweb).
* [ ] Vérifier que le matériel est mis en panne.
* [ ] Vérifier au retour a l'écran d'accueil et démarrage que l'achat de carte n'est plus disponible.
* [ ] Vérifier que le message comme quoi la CB ne sera pas debitée est affiché.
* [ ] Vérifier que la dépense n'est pas enregistrée dans la table de transaction (?).
* [ ] Vérifier que la distribution est bien enregistrée dans la table de transaction (?).
      
### Paiement NOK
      
#### Non réponse à la demande de paiement 

* [ ] Vérifier la tempo de 2 minutes.

#### Erreur CB durant la prémière partie du traitement CB

* [ ] Vérifier l'apparition du message indiquant l'erreur.
* [ ] Vérifier qu'aucune dépense n'est enregistrée dans la table d'encaissement.

#### Erreur CB durant la deuxième partie du traitement CB (distribution mais pas encaissement)

* [ ] Vérifier l'apparition du message indiquant l'erreur.
* [ ] Vérifier qu'un réglement à 0 a été fait par le terminal (mettre un libellé avec l'erreur).



Gestion du crédit restant 
-------------------------

### Avant 5 minutes

* [ ] Vérifier que la dépense est bien enregistrée dans la table de transaction.

### Apres 5 minutes

* [ ] Vérifier que la distribution (crédit perdu) est bien enregistrée dans la table de transaction.


Test des erreurs de distribution de cartes
------------------------------------------

* [ ] Le distributeur de cartes est capable de détecter les erreurs de distribution cartes.
* [ ] Le distributeur est capable de ne pas encaisser le montant CB dans ce cas.
* [ ] Si la transaction se fait en billet, il existera un crédit restant (il faut qu'il y ait un
      périphérique de distribution ou recharge opérationnel, dans le cas contraire informer l'utilisateur
      à l'aide d'une boîte de dialogue adéquoite).
* [ ] Concernant le crédit restant, il est prévu une ligne de montant minimal en distribution ou recharge carte,
      d'un montant égal au crédit restant si la ligne de sélection minimale ne le permet pas déjà.
