Distributeur
============

* [x] La date doit être à l'heure.
* [ ] Le message déroulant doit être conforme.
* [ ] La boite de pub doit être desactivée.

* [x] Si l'impression ticket n'est pas opérationnel cela doit être affiché à l'écran et l'écran impression de justification OUI/NON ne doit pas apparaître.
    * [x] Supprimer le rouleau de ticket.
    * [x] Déconnecter l'imprimante.

* [ ] Si le distributeur de carte n'est pas opérationnel cela doit être affiché à l'écran et écrit dans optiweb.
    * [x] Provoquer une panne (non distribution carte).
    * [x] Déconnecter le distributeur de carte.

* [ ] Si le distributeur de carte n'a plus de carte cela doit être affiché à l'écran.
* [ ] Si le lecteur billet ou terminal CB n'est plus opérationnel cela doit être affiché à l'écran et écrit dans optiweb.
    * [ ] Déconnecter les appareils.

* [ ] Si plus de moyen de paiement opérationnel, distributeur H.S.
* [ ] Si plus de moyens de distribution, distributeur H.S.

??? success
    | Date réalisation tests | Heure réalisation tests | Opérateur(s)    | Commentaires |
    | :--------------------- | :---------------------- | :-------------- | :----------- |
    | 2018 07 25             |                         | Alexandre SIMON |              |
    |                        |                         | Benjamin GUY    |              |

??? failure
    | Date réalisation tests | Heure réalisation tests | Opérateur(s)    | Commentaires |
    | :--------------------- | :---------------------- | :-------------- | :----------- |
    | 2018 07 25             |                         | Alexandre SIMON |              |
    |                        |                         | Benjamin GUY    |              |

Crédit restant
--------------

### Test 1 - Hopper jeton, crédit restant.

!!! attention
    Paramètrer Optiweb de facon à ce que la sélection minimale soit supérieur ou égale à 10€
    en achat jeton et recharge carte. Remplir toutes les sélections de façon à obtenir 8 sélections.

* [ ] Éffectuer un achat de jetons d'au moins 10€.
* [ ] Insérer un billet de 5€ (inférieur à la sélection choisie).
* [ ] Vérifier qu'un message informant du retour au menu apparaît au bout d'une vingtaine de secondes.
* [ ] Vérifier qu'un crédit restant égal au montant du billet introduit apparaît dans l'écran de démarrage.
* [ ] Faire une nouvelle sélection, vérifier qu'une ligne de transaction minimale égale à 5€ est ajoutée.
* [ ] Vérifier qu'une ligne informant du crédit restant apparaît en haut de l'écran de sélection.
* [ ] Vérifier que la ligne de sélection maximale à disparue à cause de l'apparition de la ligne minimale égale au crédit restant.
* [ ] Acheter pour 5€ de jetons et vérifier qu'aucun paiement n'est demandé et que le credit restant disparait de l'écran démarrage,
      de l'ecran de sélection et que la ligne de sélection du montant de 5€ a bien disparue.
* [ ] Vérifier via les logs le contenu des écritures d'encaissement et de distribution.

### Test 2 - Hopper jeton, crédit perdu.

* [ ] Refaire le Test 1 (provoquer l'apparition d'un crédit restant) mais cette fois attendre 5 minutes que le crédit restant disparaisse de l'écran.
* [ ] Vérifier dans les logs l'ecriture en base de donnée d'une perte de credit restant.

### Test 3 - Hopper jeton, crédit restant billet et complément en CB

!!! info "Objectif"
    Vérifier que l'on peut compléter un achat à partir d'un crédit restant, avec un carte bancaire.
    
* [ ] Refaire le TEST 1 pour un achat de carte, avec un billet de 5 euros, mais cette fois vérifier que le complément de 5 à 10 euros peut se faire en CB

!!! note
    Garder le ticket pour la comptabilité.

### Test 4 - Distributeur Opticard , crédit restant via erreur distribution

!!! info "Objectif"
    Vérifier l'apparition d'un crédit restant en cas d'erreur de distribution d'une carte.

* [ ] Refaire le Test 1 dans le cadre d'un achat, provoquer une panne de distribution de carte
      (avec la main, empêcher/ralentir la sortie d'une carte du distributeur).
* [ ] Vérifier qu'un crédit restant de 10€ (du montant de la carte) apparaît à l'écran de démarrage.

### Test 5 - Hopper jeton, crédit restant via erreur distribution jeton

!!! info "Objectif"
    Vérifier l’apparition d’un crédit restant en cas d’erreur de distribution de jetons.

* [ ] Refaire le TEST 1 dans le cadre d’un achat de jetons, provoquer une panne de distribution en alimentant le hopper avec un nombre insuffisant de jetons, par exemple.
* [ ] Vérifier q’un crédit restant égal au montant du nombre de jetons non distribués apparait.
* [ ] Vérifier l’apparition d’un ligne de crédit minimal.

### Test 6 - Gestion de la virgule
