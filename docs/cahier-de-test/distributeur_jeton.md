Distributeur jeton
==================

Test des erreurs de distribution de jeton
-----------------------------------------

* [ ] Le distributeur de jeton est capable de détecter les erreurs de distributions de jetons.
* [ ] Le distributeur est capable de n'encaisser en CB que le montant exact distribué.
* [ ] Si la transaction se fait en billet, il existera une crédit restant égal au montant non distribué.
      ( il faut qu'il y ait un périphérique de distribution ou recharge operationnel,
      dans le cas contraire informer l'utilisateur à l'aide d'une boîte de dialogue adéquoite).
* [ ] Concernant le crédit restant, il est prévu une ligne de montant minimal en distribution ou recharge carte,
      d'un montant égal au crédit restant si la ligne de sélection minimale ne le permet pas déjà.
