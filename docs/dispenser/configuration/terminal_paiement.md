Terminal paiement
=================


Accès menu configuration
------------------------

### Bouton maintenance

Ouvrir la **porte arrière** du <ins>distributeur</ins>.

Dans le coin supérieur gauche, <ins>appuyer</ins> puis relacher le **bouton maintenance**.

Depuis l'<ins>écran du distributeur</ins>, appuyer sur le bouton virtuel **lecteur CB**.

Puis **menu TPA**.

___

### Bouton du TPE

Ouvrir la **porte arrière** du <ins>distributeur</ins>.

Appuyer le bouton de gauche sous les prises ethernet (un bip retentit).

___

Barre de service
----------------

Lorsque la  **barre de service** affiche `menu principal`.

Sélectionner dans l'ordre les <ins>intéractions</ins> suivantes:

1. `5 - Mise en service`
1. `1 - Paramètres`
1. `8 - Barre de service`
    1. `1 - Désactivée`
    1. `2 - Activée`

___

### Configurer heure

Lorsque la  **barre de service** affiche `menu principal`.

Sélectionner dans l'ordre les <ins>intéractions</ins> suivantes:

1. `4 - Préférences`
1. `5 - Date & heure`

Rentrer le jour, le mois, l'année ainsi que l'heure du jour en cours.
___

### Communication réseau

Lorsque la **connection réseau** est éxistante et synchronisée un logo avec des <ins>fléches</ins> dans des directions opposées s'affiche en haut à gauche.

___

Afficher adresse MAC
--------------------

Lorsque la  **barre de service** affiche `menu principal`.

Sélectionner dans l'ordre les <ins>intéractions</ins> suivantes:

1. `3 - Applications`
1. `3 - POSWARE`
1. `2 - Visualisation`
1. `6 - Adresse MAC`

??? info "Exemple: résultat sortie standard"
    ADRESSE MAC:

    00 08 19 2B 97 BC

Configuration utilisation clavier
---------------------------------

Lorsque la  **barre de service** affiche `menu principal`.

Sélectionner dans l'ordre les <ins>intéractions</ins> suivantes:

1. `3 - Applications`
1. `1 - CB-EMV`
1. `7 - Initialisation`

!!! info "Résulat sortie standard"
    INITIALISATION CARTE COMMERCANT

    <STOP\> ABANDON

Une fois le résultat de la sortie standard affiché sur l'écran du TPE, <ins>éxécuter la séquence suivante</ins> pour accéder à la **configuration de l'appareil**.

!!! tip "Séquence configuration"
    1. Bouton **Point d'intérogation** (bouton bleu sur clavier).
    1. Bouton **gauche** (bouton horizontal dans bordure écran).
    1. Bouton **milieu** (bouton horizontal dans bordure écran).
    1. Bouton **droite** (bouton horizontal dans bordure écran).

Numéro | Paramètre                  | Valeur               | Information                
:----: | -------------------------: | :-----------------   | :--------------------------
**01** | **NUM COMMERCE**           | **XXX XX XX**        | **Numéro commercant ou numéro de contrat**
**02** | **Code banque**            | **XX XXX**           | **Code banque**
03     | NUM LOG SYSTEME            | 001                  | Valeur par défaut
04     | CLASSIFICATION CB          | Classe 2.1           | `2. Classe 2.1`
05     | IMP. TICKET ABANDON        | Activée              | `2. ACTIVEE`
06     | MODE IP                    | Passerelle IP/X25    | `2. Passerelle IP/X2`
07     | PASSERELLE                 | RFC1086 Aménagé      | `2. RFC amenage`
08     | PROTO. CB COM              | Proto 1.2            | `1. Proto. CBcom 1.2`
09     | TYPE ADR. PRIMAIRE         | Adresse IPv4         | `1. ADRESSE IP V4`
10     | ADR. IP PRIMAIRE           | 194 XXX XXX XXX      |
11     | PORT PRINCIPAL             | 1256                 |
**12** | **ADRES APPEL**            | **196 XXX XXX XXXX** | **Numéro de centre (196 ...)**
13     | TYPE ADR. SECONDAIRE       | Adresse IPv4         |
14     | ADR. IP SECONDAIRE         | 194 XXX XXX XXX      |
15     | PORT SECONDAIRE            | 1256                 |
**16** | **ADRES APPEL**            | **196 XXX XXX XXXX** | **Numéro de centre (196 ...)**
17     | VERIFICATION SERVEUR       | Pas de contrôle      | `2. Pas de controle`
18     | VERIFICATION LRC           | Pas de contrôle      | `2. Pas de controle`
19     | RAISON D'APPEL             | OK                   | `1. Premiere init`
20     | PREMIERE INITIALISATION    | OK                   | Démarrage communication avec la banque

Configuration utilisation NFC
-----------------------------

Lorsque la  **barre de service** affiche `menu principal`.

Sélectionner dans l'ordre les <ins>intéractions</ins> suivantes:

1. `3 - Applications`
1. `2 - CB-NFC`
1. `7 - Initialisation`

**Même procédure** que pour la configuration de [<ins>l'utilisation au clavier</ins>](#configuration-utilisation-clavier) mais numéro de commercant différent.
