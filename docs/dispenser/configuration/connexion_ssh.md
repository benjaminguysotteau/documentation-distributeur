Configuration
=============

Connexion SSH
-------------

Dans le fichier /home/phyvm/.ssh/**config**

```sh
Host  distrib-dev
  Hostname  192.168.0.15
  Port      22 # Optinal
  User      root
```

Ce qui permet de faire **ssh distrib-dev**.
