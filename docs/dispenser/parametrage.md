Paramètrage
===========

Identifiant de connexion
------------------------

`<prenom>`.`<nom>`@heurtaux.fr

ET

votre mot de passe

Formulaire de connexion
-----------------------

Se rendre sur la page d'indentification a cette adresse:

+ [Optiweb Beta](https://beta-heurtaux.siqual.net/login)

Récupération adresse IP
-----------------------

![Optiweb Beta Compteur](img/optiweb_beta_compteur.png)

1. Selectionner la machine souhaitee dans la colonne de gauche.
2. Se positionner dans l'onglet **Compteur**.
3. Lire la valeur du parametre **ADRESSE IP DISTRIBUTEUR**.

Dans l'exemple  l'ip est: *192.168.0.21*

Paramètrage périphériques
-------------------------

![Optiweb Beta Compteur](img/optiweb_beta_parametres.png)

1. Selectionner la machine souhaitee dans la colonne de gauche.
2. Se positionner dans l'onglet **Parametres**.
3. Se rendre dans la partie **Configuration machine**.

### Configuration machine

![Optiweb Beta Compteur](img/optiweb_beta_configuration_machine.png)

Modifier la valeur du booleen via **bouton edition** de la colonne action.

![Optiweb Beta Compteur](img/optiweb_beta_modification_parametre.png)

1. Le parametre nom modifie s'applique avec le **bouton ignorer** (oui = on ne modifie pas le nom, non = on mofifie le nom).
2. Le parametre valeur actuelle se modifie via le **menu deroulant** valeur modifiee.
3. La **checkbox distributeur** permet de synchroniser les nouvelles valeurs entre le **DISTRIBUTEUR-DEV** et le **DISTRIBUTEUR-FAB**.


Se rendre dans la partie **Systeme**.

### Système

![Optiweb Beta Compteur](img/optiweb_beta_systeme.png)

1. Modifier la valeur du parametre souhaite avec **bouton edition**.
2. Les valeurs correspondent aux liaisons series (RS232) et laisons du BUS CAN.
3. Pour chaqu'une des valeurs en base de donnees, elle est **inferieure de 1 unite** par rapport a la configuration hardware du materiel.
