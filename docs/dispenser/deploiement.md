Déploiement
===========

Automatisé
----------

!!! info
    Lorsque l'on se connecte au serveur OVH, on est **par défaut** dans **/home/**

```sh
senddistribovh
lockovh
echo 'DISTRIBUTEUR_V1.28' > distrib_inst.txt

cd /home/log
alive
getversion
```

À la main
---------

!!! info
    Dans cet exemple la cible est **SAS IDM LAVAGE AUTO**,
    le port du reverse ssh est le **22104** et
    l'ip du distributeur est **192.168.1.13**
  
```sh
# On se connecte au serveur OVH
# on récupére le binaire dans /home
# on l'envoie à la centrale de la station (depuis son port reverse SSH)
lockovh
cd /home
scp -P 22104 -o StrictHostKeyChecking=no DISTRIBUTEUR_V1.26 root@127.0.0.1:/tmp
scp -P 22104 -o StrictHostKeyChecking=no *.qss              root@127.0.0.1:/tmp

# On se connecte à la centrale,
# on envoie le binaire au distributeur,
# ainsi que les fichiers qss (si besoin)
cd /home/
./who_are_you.sh # LE LUC
scp DISTRIBUTEUR_V1.26  root@192.168.1.13:/tmp
scp *.qss               root@192.168.1.13:/tmp

# On se connecte au distributeur,
# on renomme le binaire éxécuté en OLD_DISTRIBUTEUR
# on renomme le binaire envoyé en DISTRIBUTEUR,
# ainsi que les fichiers qss (si besoin)
# on termine le processus DISTRIBUTEUR
# on redémarre le distributeur
ssh root@192.168.1.13
cd /home/
mv DISTRIBUTEUR             bckp_DISTRIBUTEUR_V1.15_180704_115100
mv default.qss              bckp_default.qss_180704_140100
mv default_maint.qss        bckp_default_maint.qss_180704_140100
mv /tmp/DISTRIBUTEUR_V1.26  DISTRIBUTEUR
mv /tmp/default.qss         default.qss
mv /tmp/default_maint.qss   default_maint.qss
kill -9 ./DISTRIBUTEUR
reboot
```
