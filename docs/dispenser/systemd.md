Systemd
=======

!!! info
  Dans cette documentation on réalise un timer pour le script watcher_sh_version.sh à l'aide de systemd.

Création d'un service
---------------------

1. En tant que **root** (sudo su, si nécessaire)
2. cd /etc/systemd/system
3. vi watcher_sh_service.**service**

```sh
# Contenu du fichier watcher_sh_version.service

[Service]
Type=oneshot
ExecStart=/home/watcher_sh_version.sh
```

Sauvegarder et fermer le fichier.

Création d'un service avec redémarrage
--------------------------------------

1. En tant que **root** (sudo su, si nécessaire)
2. cd /etc/systemd/system
3. vi watcher_log.**service**

```sh
[Unit]
Description=Send log files with scp from a dispenser to OVH server.

[Service]
Type=simple
User=root
ExecStart=/home/watcher_log.sh

[Install]
WantedBy=multi-user.target
```

!!! info
    Le tag **[Install]** avec la variable **WantedBy** permet d'éxécuter par la suite la commande:

    `systemctl enable watcher_log.service`

Création d'un timer
-------------------

1. En tant que **root** (sudo su, si nécessaire)
2. cd /etc/systemd/system
3. vi watcher_sh_service.**timer**

```sh
# Contenu du fichier watcher_sh_version.timer

[Timer]
# Tous les 10 minutes.
OnCalendar=*:0/10
AccuracySec=1us
```

Activation d'un service
-----------------------

En tant que **root** (sudo su, si nécessaire)

```sh
systemctl daemon-reload
systemctl start watcher_sh_version.timer
```

Activation d'un service avec redémarrage
----------------------------------------

!!! attention
    Pour pouvoir utiliser **enable** il faut au préalable avoir créer un service avec un tag **[Install]**.

En tant que **root** (sudo su, si nécessaire)

```sh
systemctl daemon-reload
systemctl enable watcher_log.service
systemctl start  watcher_log.service
```

Lister les timers de systemd
----------------------------

```sh
systemctl list-timers
```

Arrêt d'un service
------------------

En tant que **root** (sudo su, si nécessaire)

```sh
systemctl stop watcher_sh_version.timer
```

Redémarrer le service après crash
---------------------------------

```sh
systemctl stop    watcher_sh_version.timer
systemctl start   watcher_sh_version.timer
systemctl status  watcher_sh_version.timer
systemctl status  watcher_sh_version.service
```

Sources
-------

1. [blog.octo.com systemd](https://blog.octo.com/5-services-que-systemd-ma-deja-rendu/)
2. [stackexchange systemd](https://unix.stackexchange.com/questions/126786/systemd-timer-every-15-minutes)
