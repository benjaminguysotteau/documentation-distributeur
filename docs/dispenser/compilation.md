Compilation
===========

Locale
------

Si Qt Creator est ouvert depuis la commande **pqt &** dans un terminal Bash, imperativement le quitter!

1. Ouvrir Qt creator depuis le **docker ou la recherche d'applications**.
2. En bas a gauche cliquer le bouton au dessus de la fleche verte (**icone ordinateur ou carte electronique**).
3. Selectionner le **kit Desktop** Qt 5.3 GCC 64 bit.
4. Selectionner **Debug**.

![Qt Creator compilation locale](img/qt_creator_compilation_locale.png)

Dans le fichier source const.h.

A la ligne 16 commenter le **define PROD**.

![Qt Creator compilation production](img/qt_creator_define_locale.png)

Ouvrir le menu Build.

![Qt Creator build](img/qt_creator_build.png)

Depuis le menu Build, faire une iteration de chacune des actions suivantes:

1. Clean All
2. Run Qmake
3. Build All

Production
----------

Si Qt Creator est ouvert depuis le docker ou la recherche d'applications, imperativement le quitter!

1. Ouvrir Qt creator depuis depuis un terminal Bash est executer la commande: **pqt &**.
2. En bas a gauche cliquer le bouton au dessus de la fleche verte (**icone ordinateur ou carte electronique**).
3. Selectionner le **PhyBOARD-Wega**.
4. Selectionner **Debug**.

![Qt Creator compilation production](img/qt_creator_compilation_production.png)

Dans le fichier source const.h.

A la ligne 16 decommenter le **define PROD**.

![Qt Creator compilation production](img/qt_creator_define_production.png)

Ouvrir le menu Build.

![Qt Creator build](img/qt_creator_build.png)

Depuis le menu Build, faire une iteration de chacune des actions suivantes:

1. Clean All
2. Run Qmake
3. Build All

Envoyer la nouvelle distribution
--------------------------------

Depuis un terminal **Bash**.

Si l'ip du distributeur est en 192.168.0.15.

```sh
getdistrib
```

Sinon
```sh
getdistribto 15
```
